import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {UserProvider} from "../../providers/user/user";

@IonicPage()
@Component({
  selector: 'page-thanks',
  templateUrl: 'thanks.html',
})
export class ThanksPage {
  rootPage: any;
  data: any;

  constructor(private nav: NavController, public navParams: NavParams,public user: UserProvider) {
    this.data = this.navParams.data.params;
    this.sendMsg();
  }

  goHome(){
    this.nav.popToRoot();
    this.nav.parent.select(0);
  }

  sendMsg(){
      this.user.sendSMS('966' + this.data.billing.phone, 'شكرا ' + this.data.billing.first_name + ',\n' + 'لطلبك من لحوم فطوم, سوف يتم التواصل معك.\n الرقم المرجعي للطلب:' + this.data.id).subscribe((res) => {});
  }

}

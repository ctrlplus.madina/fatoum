import { Component } from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-account-help',
  templateUrl: 'help.html',
})
export class AccountHelpPage {
  faqs: any[] = [
    {id: 1, title: 'كيف سأضع الطلب؟', answer: 'فقط قم بالتسجيل وحدد المنتج ثم سجل عنوانك واختر طريقة الدفع الخاصة بك.'},
    {id: 2, title: 'كيف سأضع الطلب؟', answer: 'فقط قم بالتسجيل وحدد المنتج ثم سجل عنوانك واختر طريقة الدفع الخاصة بك.'},
    {id: 3, title: 'كيف سأضع الطلب؟', answer: 'فقط قم بالتسجيل وحدد المنتج ثم سجل عنوانك واختر طريقة الدفع الخاصة بك.'},
    {id: 4, title: 'كيف سأضع الطلب؟', answer: 'فقط قم بالتسجيل وحدد المنتج ثم سجل عنوانك واختر طريقة الدفع الخاصة بك.'},

  ];

  constructor(public view: ViewController) {
  
  }

  toggleSection(i) {
    this.faqs[i].open = !this.faqs[i].open;
  }

  toggleItem(i, j) {
    this.faqs[i].child[j].open = !this.faqs[i].child[j].open;
  }

    dismiss() {
        this.view.dismiss();
    }

}

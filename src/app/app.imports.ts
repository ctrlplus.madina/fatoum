// Ionic native providers
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {IonicStorageModule} from '@ionic/storage';

import {AppRate} from '@ionic-native/app-rate';
import {AppVersion} from '@ionic-native/app-version';

// Modules
import {Http, HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {IonicImageViewerModule} from 'ionic-img-viewer';

// Directives
import {ParallaxHeader} from '../directives/parallax-header/parallax-header';

// Components
import {IonRating, FaIconComponent} from '../components/components';

// Pipes
import {MomentPipe, DiscountPipe, MoneyPipe} from '../pipes/pipes';

// Providers
import {
    UserProvider,
    ToastProvider,
    SettingsProvider,
    LoadingProvider,
    // NotifProvider,
    AddressProvider,
    OrderProvider,
    WooCommerceProvider,
    CartProvider,
    WishlistProvider,
    HistoryProvider
} from '../providers/providers';
import {CustomProvider} from "../providers/custom/custom";
import {HttpClientModule} from "@angular/common/http";
import {Firebase} from "@ionic-native/firebase";
import {GoogleMaps} from "@ionic-native/google-maps";
import {Diagnostic} from "@ionic-native/diagnostic";
import {NativeGeocoder} from "@ionic-native/native-geocoder";
import {CallNumber} from "@ionic-native/call-number";
import {Contacts} from "@ionic-native/contacts";
import {SocialSharing} from "@ionic-native/social-sharing";
import {DatePicker} from "@ionic-native/date-picker";

export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/languages/', '.json');
}

export const MODULES = [
    HttpModule,
    BrowserModule,
    HttpClientModule,
    IonicImageViewerModule,
    // database name for cart, wish, checkout, etc
    IonicStorageModule.forRoot(),
    // IonicStorageModule.forRoot({
    //     name: '__fatoumsql',
    //     driverOrder: ['indexeddb', 'sqlite', 'websql']
    // }),
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: (createTranslateLoader),
            deps: [Http]
        }
    })
];

export const PIPES = [
    MomentPipe,
    DiscountPipe,
    MoneyPipe
];

export const PROVIDERS = [
    //AppState,
    UserProvider,
    OrderProvider,
    AddressProvider,
    WooCommerceProvider,
    SettingsProvider,
    WishlistProvider,
    CartProvider,
    HistoryProvider,
    LoadingProvider,
    // NotifProvider,
    ToastProvider,
    CustomProvider,
    GoogleMaps,
    Diagnostic,
    NativeGeocoder,
    CallNumber,
    Contacts,
    AppRate,
    SocialSharing,
    DatePicker,
    // Ionic native specific providers
    IonicStorageModule,
    StatusBar,
    SplashScreen,
    AppRate,
    AppVersion,
    Firebase
];

export const COMPONENTS = [
    IonRating,
    FaIconComponent
];

export const DIRECTIVES = [
    ParallaxHeader
];
import {Component} from '@angular/core';
import {AppRate} from '@ionic-native/app-rate';
import {AppVersion} from '@ionic-native/app-version';
import {IonicPage, AlertController, ModalController, Platform, NavController, NavParams} from 'ionic-angular';
import {UserProvider, AddressProvider} from '../../providers/providers';
import {App} from '../../app/app.global';
import {TranslateService} from '@ngx-translate/core';
import {CallNumber} from "@ionic-native/call-number";
import {
    Contact, ContactField, ContactFieldType, ContactFindOptions, ContactName,
    Contacts
} from "@ionic-native/contacts";
import {SocialSharing} from "@ionic-native/social-sharing";

@IonicPage()
@Component({
    selector: 'page-account',
    templateUrl: 'account.html',
})
export class AccountPage {
    notif: any;
    address: any;
    user: any;
    app: any;
    temppic: any;

    constructor(private appVersion: AppVersion, private appRate: AppRate, private translate: TranslateService, private alert: AlertController, private platform: Platform, public navCtrl: NavController, private _address: AddressProvider, private modal: ModalController, private _user: UserProvider, public navParams: NavParams,
               private callNumber: CallNumber, private contacts: Contacts,private socialSharing: SocialSharing) {
        // this.notif = this._notif;
        this.address = this._address;
        this.user = this._user;

        //console.log(this.user);

        this.app = App;
        if (this.platform.is('cordova')) {
            this.appVersion.getVersionNumber().then(res => {
                this.app.version = res;
            })
        }
        this.findContact();
    }

    ionViewDidLoad() {

    }

    rateUs() {
        if (!this.platform.is('cordova')) {
            this.translate.get(['OK', 'ONLY_DEVICE', 'ONLY_DEVICE_DESC']).subscribe(x => {
                this.alert.create({
                    title: x.ONLY_DEVICE,
                    message: x.ONLY_DEVICE_DESC,
                    buttons: [{text: x.OK}]
                }).present();
                return false;
            });

        } else {
            this.appVersion.getAppName().then(res => {
                this.appRate.preferences.displayAppName = res;
            });

            this.appVersion.getPackageName().then(res => {
                this.appRate.preferences.storeAppURL = {
                    ios: 'com.fatoum.ctrlplus', // FOR IOS
                    android: 'market://details?id=' + res, // FOR ANDROID, use your own android package name
                    // windows: 'ms-windows-store://review/?ProductId=<store_id>' // FOR WINDOWS APP
                };
                this.appRate.promptForRating(true);
            })
        }
    }

    goProfile() {
        this.modal.create('AccountProfilePage').present();
    }

    goTo(page, params) {
        this.navCtrl.push(page, {params: params});
    }


    login() {
        this.navCtrl.push('LoginPage');
        // this.modal.create('LoginPage', {}).present();
    }

    logout() {
        this.translate.get(['LOGOUT', 'LOGOUT_MSG', 'YES', 'CANCEL']).subscribe(x => {
            let confirm = this.alert.create({
                title: x.LOGOUT,
                message: x.LOGOUT_MSG,
                buttons: [{
                    text: x.CANCEL
                }, {
                    text: x.YES,
                    handler: () => {
                        this.confirmLogout();
                    }
                }]
            });
            confirm.present();
        });

    }

    confirmLogout() {


        // if (this.user.type_login == 'fb') {
        //
        //     // this.fb_logout();
        //
        // } else if (this.user.type_login == 'tw') {
        //
        // } else {


            this.user.logout().then(() => {
                this.navCtrl.popToRoot();
                this.navCtrl.parent.select(0);
            });
     //   }
    }

    // fb_logout() {
    //     let successCallback = (success) => {
    //         console.log(success);
    //         if (success == 'OK') {
    //             this.user.logout().then(() => {
    //                 this.navCtrl.popToRoot();
    //                 this.navCtrl.parent.select(0);
    //             });
    //         }
    //     };
    //     let errorCallback = (e) => console.error(e);
    //     this.facebook.logout().then(successCallback).catch(errorCallback);
    // }

    createContact() {

        let contact: Contact = this.contacts.create();

        contact.name = new ContactName(null, 'لحوم فطوم');
        contact.phoneNumbers = [new ContactField('mobile', '+966593030494')];
        contact.save().then(
            () => console.log('Contact saved!', contact),
            (error: any) => console.error('Error saving contact.', error)
        );
    }


    findContact() {

        let options = new ContactFindOptions();
        options.filter = "+966593030494";
        options.multiple = true;
        options.hasPhoneNumber = true;
        let filter: ContactFieldType[] = ["phoneNumbers"];


        this.contacts.find(filter, options).then(
            (val) => {
                console.log('Contact found!', val);
                if (val.length <= 0) {
                    this.createContact();
                }
            },
            (error: any) => console.error('Error saving contact.', error)
        );
        ;
    }

    callUs() {
        this.callNumber.callNumber("+966593030494", true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    regularWhatsappShare() {

        this.socialSharing.shareViaWhatsAppToReceiver('+966593030494', 'السلام و عليكم!\n', null, null);
    }

}


import {Component, ElementRef, ViewChild} from '@angular/core';
import {IonicPage, NavController, NavParams, ToastController, ViewController} from 'ionic-angular';
import {
    WooCommerceProvider,
    ToastProvider,
    UserProvider,
    OrderProvider,
    AddressProvider
} from '../../../../providers/providers';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {
    GoogleMaps,
    GoogleMap,
    Marker,
    GoogleMapsAnimation
} from '@ionic-native/google-maps'
import {Diagnostic} from "@ionic-native/diagnostic";
import {NativeGeocoder, NativeGeocoderReverseResult} from "@ionic-native/native-geocoder";


@IonicPage()
@Component({
    selector: 'page-add-address',
    templateUrl: 'add-address.html',
})
export class AddAddressPage {
    @ViewChild('map') mapElement: ElementRef;
    map: GoogleMap;
    private form: FormGroup;

    data: any;
    countries: any[] = [];
    states: any[] = [];
    countryOpts: any;
    stateOpts: any;


    constructor(public viewCtrl: ViewController, private toast: ToastProvider, private translate: TranslateService, private user: UserProvider, private order: OrderProvider, private navParams: NavParams, private address: AddressProvider, private fb: FormBuilder, private woo: WooCommerceProvider,
                private diagnostic: Diagnostic, public toastCtrl: ToastController, private nativeGeocoder: NativeGeocoder, public nav: NavController) {
        this.translate.get(['SELECT', 'COUNTRY', 'SELECT_YOUR_COUNTRY', 'STATE', 'SELECT_YOUR_STATE']).subscribe(x => {
            this.countryOpts = {
                title: x.SELECT + ' ' + x.COUNTRY,
                subTitle: x.SELECT_YOUR_COUNTRY
            }

            this.stateOpts = {
                title: x.SELECT + ' ' + x.STATE,
                subTitle: x.SELECT_YOUR_STATE
            }
        });

        this.data = navParams.data.params;

        this.getStates();

        // this.loadCountry();
        this.data.state = 'المدينةالمنورة';
        this.data.city = 'المدينةالمنورة';
        this.data.country = 'المملكة العربية السعودية';
        if (!this.data.address_2) {
            this.data.lat = 0;
            this.data.lng = 0;
        } else {
            this.data.lat = this.data.address_2.split(',')[0];
                this.data.lng = this.data.address_2.split(',')[1];
        }


        this.form = this.fb.group({
            first_name: [this.data.first_name || (this.user.all ? this.user.firstname : ''), Validators.required],
            last_name: [this.data.first_name || (this.user.all ? this.user.firstname : ''), Validators.required],
            // last_name: [this.data.first_name || this.data.last_name ||  (this.user.all ? this.user.lastname : ''), Validators.required],
            email: [this.user.all ? this.user.email : '', Validators.required],
            phone: [this.data.phone || (this.user.all ? this.user.phone : ''), Validators.required],
            city: [this.data.city, Validators.required],
            state: [this.data.state, Validators.required],
            postcode: [this.data.postcode, Validators.required],
            address_1: [this.data.address_1, Validators.required],
            country: [this.data.country, Validators.required],
            address_2: [this.data.lat + ',' + this.data.lng, Validators.required],
        });




    }

    ionViewDidEnter() {

        this.loadMap();
        this.checkPermission();
    }


    // loadCountry(){
    //   // if(this.setting.all.countries){
    //   //   this.countries = this.setting.getCountries();
    //   // }else{
    //     this.loader.present();
    //     this.woo.saveCountries().then( val=> {
    //       if(val) {
    //         if(val.value.length == 0){
    //           for(let i in val.options)
    //             this.countries.push({id: i, name: val.options[i]});
    //         }else{
    //           for(let i in val.value)
    //             this.countries.push({id: i, name: val.value[i]});
    //         }
    //           console.log(this.countries);
    //       }
    //       this.loader.dismiss();
    //     }, err=> {
    //       this.loader.dismiss();
    //       console.log(err);
    //     })
    // //  }
    //
    //   if(this.data.action == 2)
    //     this.getStates();
    //
    //
    // }

    getStates() {
        this.data.country = 'SA';
        let id = this.data.country;
        // let id = this.data.country || this.form.value.country;


        this.woo.getStates(id).map(res => res.json()).subscribe(res => {
            for (let i in res)
                this.states.push({id: i, name: res[i]});
        }, err => {
            console.error("Error : " + err);
            this.states = [];
        });
    }

    submit() {
        if (this.address.all.length == 0)
            this.form.value.primary = true;

        if (this.data.primary)
            this.form.value.primary = this.data.primary;

        if (this.data.action == 1) {
            this.address.add(this.form.value); // FOR NEW ADDRESS
            this.translate.get('NEW_ADDRESS_ADDED').subscribe(x => {
                this.toast.show(x);
            });
        }

        if (this.data.action == 2) {
            this.address.update(this.navParams.data.index, this.form.value); // UPDATE ADDRESS
            this.translate.get('ADDRESS_WAS_UPDATED').subscribe(x => {
                this.toast.show(x);
            });
        }

        if (this.data.action == 3) { // FOR CHECKOUT BILLING
            this.address.add(this.form.value);
            this.order.setBilling(this.form.value);
        }

        if (this.data.action == 4) { // FOR CHECKOUT SHIPPING
            this.address.add(this.form.value);
            this.order.setShipping(this.form.value);
        }

        this.dismiss();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }


    checkPermission() {
        let successCallback = (status) => {

            switch (status) {
                case this.diagnostic.permissionStatus.NOT_REQUESTED:
                    //    console.log("Permission not requested");
                    break;
                case this.diagnostic.permissionStatus.DENIED:
                    // console.log("Permission denied");
                    break;
                case this.diagnostic.permissionStatus.GRANTED:
                    // console.log("Permission granted always");
                    break;
                case this.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
                    // console.log("Permission granted only when in use");
                    break;
            }

        };
        let errorCallback = (e) => console.error(e);

        this.diagnostic.requestLocationAuthorization().then(successCallback).catch(errorCallback);
    }

    checkLocationOnOff() {
        let successCallback = (isAvailable) => {
            if (isAvailable) {
                this.myLocation();
            } else {

                this.diagnostic.switchToLocationSettings();

                this.nav.pop();
            }
        };
        let errorCallback = (e) => console.error(e);
        this.diagnostic.isLocationAvailable().then(successCallback).catch(errorCallback);

    }

    loadMap() {


        // Create a map after the view is loaded.
        // (platform is already ready in app.component.ts)

        if (!this.data.address_2) {
            this.map = GoogleMaps.create('map_canvas', {

                camera: {
                    target: {
                        lat: 24.4710078,
                        lng: 39.47747
                    },
                    // zoom: 14,
                    tilt: 30
                }
            });
        } else {
            this.map = GoogleMaps.create('map_canvas', {

                camera: {
                    target: {
                        lat: this.data.address_2.split(',')[0],
                        lng: this.data.address_2.split(',')[1],
                    },
                    zoom: 17,
                    tilt: 30
                }
            });

            this.map.animateCamera({
                target: {
                    lat: this.data.address_2.split(',')[0],
                    lng: this.data.address_2.split(',')[1],
                },
                zoom: 17,
                tilt: 30
            })
                .then(() => {
                    // add a marker
                    let marker: Marker = this.map.addMarkerSync({
                        title: 'موقعك الحا',
                        // snippet: 'This plugin is awesome!',
                        position: {
                            lat: this.data.address_2.split(',')[0],
                            lng: this.data.address_2.split(',')[1],
                        },
                        animation: GoogleMapsAnimation.BOUNCE
                    });

                    // show the infoWindow
                    marker.showInfoWindow();
                    //
                    // // If clicked it, display the alert
                    // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                    //     this.showToast('clicked!');
                    // });
                });
        }

    }

    myLocation() {

        this.map.clear();

        // Get the location of you


        GoogleMaps.getPlugin().LocationService.getMyLocation({enableHighAccuracy: true, timeout: 3000}).then
        ((result) => {

                // console.log(JSON.stringify(result));

                this.data.lat = result.latLng.lat;
                this.data.lng = result.latLng.lng;


                // console.log(JSON.stringify(location, null ,2));
                this.nativeGeocoder.reverseGeocode(result.latLng.lat, result.latLng.lng)
                    .then((result: NativeGeocoderReverseResult) => {
                            result = result[0];
                            if (result.locality == 'Medina' || result.locality == 'المدينة المنورة') {
                                this.data.state = 'المدينةالمنورة';
                                this.data.city = 'المدينةالمنورة';
                                this.data.country = 'المملكة العربية السعودية';
                                this.data.postcode = result.postalCode;
                                this.data.address_1 = JSON.stringify(result);
                                this.data.address_1 = this.data.address_1.replace('{', '').replace('}', '').replace(new RegExp(',', 'g'), ',\n');
                                this.form = this.fb.group({
                                    first_name: [this.data.first_name || (this.user.all ? this.user.firstname : ''), Validators.required],
                                    last_name: [this.data.last_name || this.data.first_name || (this.user.all ? this.user.lastname : ''), Validators.required],
                                    email: [this.user.all ? this.user.email : '', Validators.required],
                                    phone: [this.data.phone || (this.user.all ? this.user.phone : ''), Validators.required],
                                    city: [this.data.city, Validators.required],
                                    state: [this.data.state, Validators.required],
                                    postcode: [this.data.postcode, Validators.required],
                                    address_1: [this.data.address_1, Validators.required],
                                    address_2: [this.data.lat + ',' + this.data.lng, Validators.required],
                                    country: [this.data.country, Validators.required],
                                });
                                this.translate.get('SUCCESSGETLOCATION').subscribe(x => {
                                    this.toast.show(x);
                                });
                            }else{
                                this.translate.get('DELIVERONLYMADINA').subscribe(x => {
                                    this.toast.show(x);
                                });
                            }
                            // console.log(JSON.stringify(result));
                            // console.log(result);
                        }
                    )
                    .catch((error: any) => console.log(error));


                // Move the map camera to the location with animation
                this.map.animateCamera({
                    target: result.latLng,
                    zoom: 17,
                    tilt: 30
                })
                    .then(() => {
                        // add a marker
                        let marker: Marker = this.map.addMarkerSync({
                            title: 'موقعك الحا',
                            // snippet: 'This plugin is awesome!',
                            position: result.latLng,
                            animation: GoogleMapsAnimation.BOUNCE
                        });

                        // show the infoWindow
                        marker.showInfoWindow();
                        //
                        // // If clicked it, display the alert
                        // marker.on(GoogleMapsEvent.MARKER_CLICK).subscribe(() => {
                        //     this.showToast('clicked!');
                        // });
                    });
            },
            (err) => {
                console.error('err');
                console.error(JSON.stringify(err));
            }).catch((e) => {
            console.error('catch');
            console.error(JSON.stringify(e));
        });
    }

    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: 'middle'
        });

        toast.present(toast);
    }


    // loc() {
    //     GoogleMaps.getPlugin().LocationService.getMyLocation({enableHighAccuracy: true}).then(
    //         (result) => {
    //             console.error('result');
    //             console.log(JSON.stringify(result));
    //         },
    //         (err) => {
    //
    //             console.error(JSON.stringify(err));
    //         }
    //     ).catch((e) => {
    //
    //         console.error(JSON.stringify(e));
    //     });
    // }
}




import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {WooCommerceProvider} from '../../providers/providers';

@IonicPage()
@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html',
})
export class CategoriesPage {
    categories: any = [];
    zone: NgZone;
    parentcategory: any;
    constructor(private nav: NavController, private woo: WooCommerceProvider,private navParam: NavParams) {
        //  this.loader.present();
        this.parentcategory = this.navParam.data.params;
        // console.log(this.parentcategory);
        this.zone = new NgZone({enableLongStackTrace: false});
        this.woo.getAllCategoriesParent(this.parentcategory.id).then((val) => {

            this.zone.run(() => {
                this.categories = [];
                this.categories = val;
                //console.log(this.categories);
                // this.loader.dismiss();
            });
        });
    }


    toggleSection(i) {
        this.categories[i].open = !this.categories[i].open;
    }

    toggleItem(i, j) {
        this.categories[i].child[j].open = !this.categories[i].child[j].open;
    }

    goTo(page, params) {
        this.nav.push(page, {params: params});
    }

    goToTabs(page, params) {
        this.nav.push(page, {tabIndex: params});
    }

}

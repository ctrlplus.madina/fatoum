import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, ModalController, ActionSheetController} from 'ionic-angular';
import {WooCommerceProvider, ToastProvider, WishlistProvider} from '../../providers/providers';
import {TranslateService} from '@ngx-translate/core';
import {App} from '../../app/app.global';
import {CustomProvider} from "../../providers/custom/custom";
import {
    Contact, ContactField, ContactFieldType, ContactFindOptions, ContactName,
    Contacts
} from "@ionic-native/contacts";
import {CallNumber} from "@ionic-native/call-number";
import {SocialSharing} from "@ionic-native/social-sharing";


@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {

    App: any;
    categories: any[] = [];
    data: any[] = [];
    app: any;
    sliders: any;
    zone: NgZone;

    constructor(public nav: NavController, private translate: TranslateService, private toast: ToastProvider, public wishlist: WishlistProvider, public modalCtrl: ModalController, private woo: WooCommerceProvider, private CP: CustomProvider,
                public actionSheetCtrl: ActionSheetController,private callNumber: CallNumber, private contacts: Contacts,private socialSharing: SocialSharing) {
        //	this.loader.present();
        this.App = App;
        this.zone = new NgZone({enableLongStackTrace: false});
        this.data = [];
        this.categories = []


        this.woo.getAllCategoriesParent(0).then((tmp) => {
            this.zone.run(() => {
                this.categories = tmp;
                // console.log(this.categories);
                //
                // let c = this.categories.filter(
                //     category => category.id === 15);


                this.categories.splice(0, 1);


                this.woo.loadSetting().then(x => {
                    if (x.currency) {
                        this.app = x;

                        // for (let i in tmp) {

                        // if (tmp[i].count > 1 && tmp[i].parent != 0) {
                        //     this.woo.getAllProducts(null, null, null, null, null, 5, null, null).then((val) => {
                        //
                        //         // val.forEach(function (value) {
                        //         //         console.log(value)
                        //         //     // value['category_name'] = value.name;
                        //         //     // value['category_id'] = value.id;
                        //         // });
                        //
                        //
                        //         this.data=val;
                        //         // this.data.push(val);
                        //         // console.log(val);
                        //
                        //     })
                    }
                    //}

                    //	this.loader.dismiss();

                    //}
                });
            });

        });
        this.CP.getData('slider/sliderapi.php').subscribe(val => {

            this.zone.run(() => {
                this.sliders = [];
                this.sliders = val;

            });
        });


    }

    setFav(product: any) {
        this.translate.get(['REMOVE_WISH', 'ADDED_WISH']).subscribe(x => {
            let msg = product.isFav ? x.REMOVE_WISH : x.ADDED_WISH;
            this.wishlist.post(product);
            product.isFav = product.isFav ? false : true;
            this.toast.show(msg);
        });
    }

    showSearch() {
        this.modalCtrl.create('SearchPage').present();
    }

    goTo(page, params) {

        this.nav.push(page, {params: params});
    }

    contactActionSheet() {
        this.findContact();
        this.translate.get(['CALUS', 'MSGUS','CONTACT','CANCEL']).subscribe(x => {
            const actionSheet = this.actionSheetCtrl.create({

                title: x.CONTACT,
                buttons: [
                    {
                        text: x.CALUS,
                        icon: 'call',
                        cssClass: 'call-color',
                        handler: () => {
                            this.callUs();
                            // console.log('CALUS clicked');
                        }
                    }, {
                        text: x.MSGUS,
                        icon: 'logo-whatsapp',
                        cssClass : 'whatsapp-color',
                        handler: () => {
                            //for Android
                            this.regularWhatsappShare();

                            //for IOS
                            // window.location.href = "https://wa.me/966593030494";

                            //console.log('MSGUS clicked');
                        }
                    },
                    {
                        text: x.CANCEL,
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });

            actionSheet.present();
        });
    }

    regularWhatsappShare() {

        this.socialSharing.shareViaWhatsAppToReceiver('+966593030494', 'السلام و عليكم!\n', null, null);
    }

    callUs() {
        this.callNumber.callNumber("+966593030494", true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
    }

    createContact() {

        let contact: Contact = this.contacts.create();

        contact.name = new ContactName(null, 'لحوم فطوم');
        contact.phoneNumbers = [new ContactField('mobile', '+966593030494')];
        contact.save().then(
            () => console.log('Contact saved!', contact),
            (error: any) => console.error('Error saving contact.', error)
        );
    }


    findContact() {

        let options = new ContactFindOptions();
        options.filter = "+966593030494";
        options.multiple = true;
        options.hasPhoneNumber = true;
        let filter: ContactFieldType[] = ["phoneNumbers"];


        this.contacts.find(filter, options).then(
            (val) => {
                console.log('Contact found!', val);
                if (val.length <= 0) {
                    this.createContact();
                }
            },
            (error: any) => console.error('Error saving contact.', error)
        );
        ;
    }

}

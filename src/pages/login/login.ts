import {Component, ViewChild} from '@angular/core';
import {AlertController, App, NavParams, ViewController, Slides, IonicPage} from 'ionic-angular';
import {UserProvider, ToastProvider, LoadingProvider} from '../../providers/providers';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';
import {timer} from "rxjs/observable/timer";
import {take, map} from 'rxjs/operators';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    @ViewChild('slider') slider: Slides;
    @ViewChild('innerSlider') innerSlider: Slides;

    private loginForm: FormGroup;
    private signupForm: FormGroup;
    private resetForm: FormGroup;
    private verificationForm: FormGroup;
    phoneNumber: Number;
    countDown;
    count = 100;

    constructor(private fb: FormBuilder, private translate: TranslateService, private navParams: NavParams, private toast: ToastProvider, public user: UserProvider, public loader: LoadingProvider, public alertCtrl: AlertController, public app: App, public viewCtrl: ViewController) {
        this.loginForm = this.fb.group({
            user: ['', Validators.required],
            // pass: ['', Validators.required]
        });

        this.verificationForm = this.fb.group({
            code: ['', Validators.required]
        });


        this.signupForm = this.fb.group({
            name: ['', Validators.required],
            // user: ['', Validators.required],
            email: ['', Validators.email],
            phone: ['', Validators.required],
            // pass: ['', Validators.required],
            // pass2: ['', Validators.required],
            picture: ['user.png'],
            type_login: ['wp']
        })

        this.resetForm = this.fb.group({
            email: ['', Validators.email]
        });


    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    goToLogin() {
        this.slider.slideTo(1);
    }

    goToSignup() {
        this.slider.slideTo(2);
    }

    goToVerification(phone: Number) {
        this.phoneNumber = phone;
        this.counter();
        this.slider.slideTo(3);
    }

    slideNext() {
        this.innerSlider.slideNext();
    }

    slidePrevious() {
        this.innerSlider.slidePrev();
    }

    submitVerification() {
        this.loader.present();
        let tmp = {
            user: this.phoneNumber,
            pass: this.phoneNumber
        }
        this.user.login(tmp).map(res => res.json())
            .subscribe(res => {

                if (res.status == 'ok') {

                    this.user.get_meta_verify(res, 'verification_code').map(ver => ver.json()).subscribe((ver) => {


                        if (ver['verification_code'][0] == this.verificationForm.value.code) {

                            this.user.update_meta(res, 'verification', 'yes').subscribe((ver_status) => {
                                this.translate.get(['VERIFICATION_MSG_SENT'], {value: this.signupForm.value.name}).subscribe(x => {
                                    this.toast.show(x.VERIFICATION_MSG_SENT);
                                });
                                this.user._loggedIn(res, this.navParams.data.tabIndex);
                                this.user.get_meta('phone');
                                this.user.get_meta('picture');
                                this.user.get_meta('type_login');
                                this.translate.get(['LOGIN_SUCCESS'], {value: this.user.name}).subscribe(x => {
                                    this.toast.show(x.LOGIN_SUCCESS);
                                });
                                this.dismiss();
                            });

                        } else {
                            this.translate.get(['WRONGCODE']).subscribe(x => {
                                this.toast.show(x.WRONGCODE);
                            });
                        }

                    });

                    this.loader.dismiss();
                } else {
                    this.loader.dismiss();
                    this.toast.show(res.error);
                }
            }, err => {

                this.loader.dismiss();
                this.toast.show(err.json().error);
            });
    }

    counter() {
        this.countDown = timer(0, 1000).pipe(
            take(this.count),
            map(() => --this.count)
        );
    }

    submitSignup() {
        this.loader.present();
        let tmp = {
            user: this.signupForm.value.phone,
            pass: this.signupForm.value.phone
        }
        this.user.nonce('user', 'register').map(x => x.json()).subscribe(x => {
            this.signupForm.value.nonce = x.nonce;
            this.user.signup(this.signupForm.value).map(y => y.json())
                .subscribe((y) => {


                    this.user.login(tmp).map(res => res.json())
                        .subscribe(res => {

                            if (res.status == 'ok') {
                                let vc = this.getRandomInt(10000, 99999);
                                // let vc = '93119';
                                // this.user._loggedIn(res, this.navParams.data.tabIndex);

                                this.user.update_meta(res, 'phone', this.signupForm.value.phone).subscribe();
                                this.user.update_meta(res, 'picture', this.signupForm.value.picture).subscribe();
                                this.user.update_meta(res, 'type_login', this.signupForm.value.type_login).subscribe();
                                this.user.update_meta(res, 'verification', 'no').subscribe();
                                this.user.update_meta(res, 'verification_code', vc).subscribe();


                                // this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                                //     this.toast.show(x.REGIST_SUCCESS);
                                // });
                                this.goToVerification(this.signupForm.value.phone);
                                 this.user.sendSMS('966' + this.signupForm.value.phone, 'كود التفعيل الخاص بك للحوم لفطوم هو ' + vc).subscribe((msg) => {
                                //     //this.user.sendSMS('966' + this.signupForm.value.phone, 'Your verification code of Fatoum ' + vc).subscribe((msg) => {
                                //
                                    this.translate.get(['VERIFICATION_MSG_SENT'], {value: this.signupForm.value.name}).subscribe(x => {
                                        this.toast.show(x.VERIFICATION_MSG_SENT);
                                    });

                                });


                                //  this.dismiss();
                                this.loader.dismiss();
                            } else {
                                this.loader.dismiss();
                                this.toast.show(res.error);
                            }
                        }, err => {

                            this.loader.dismiss();
                            this.toast.show(err.json().error);
                        });
                }, err => {

                    if (err.json().error == 'Username already exists.') {

                        this.loader.dismiss();
                        this.translate.get(['LOGINANDVERIFY'], {value: this.signupForm.value.name}).subscribe(x => {
                            this.toast.show(x.LOGINANDVERIFY);
                        });


                        // this.user.login(tmp).map(res => res.json())
                        //     .subscribe(res => {
                        //
                        //         if (res.status == 'ok') {
                        //             // let vc = this.getRandomInt(10000, 99999);
                        //             let vc = '93118';
                        //             // this.user._loggedIn(res, this.navParams.data.tabIndex);
                        //             this.user.get_meta_verify(res, 'verification').map(ver => ver.json()).subscribe((ver) => {
                        //
                        //
                        //                 if (ver['verification'][0] == 'no') {
                        //                     this.user.update_meta(res, 'phone', this.signupForm.value.phone).subscribe();
                        //                     this.user.update_meta(res, 'picture', this.signupForm.value.picture).subscribe();
                        //                     this.user.update_meta(res, 'type_login', this.signupForm.value.type_login).subscribe();
                        //                     this.user.update_meta(res, 'verification', 'no').subscribe();
                        //                     this.user.update_meta(res, 'verification_code', vc).subscribe();
                        //
                        //
                        //                     // this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                        //                     //     this.toast.show(x.REGIST_SUCCESS);
                        //                     // });
                        //
                        //                     // this.user.sendSMS('966' + this.signupForm.value.phone, 'Your verification code of Fatoum is ' + vc).subscribe((res) => {
                        //
                        //                     this.translate.get(['VERIFICATION_MSG_SENT'], {value: this.signupForm.value.name}).subscribe(x => {
                        //                         this.toast.show(x.VERIFICATION_MSG_SENT);
                        //                     });
                        //                     this.goToVerification();
                        //                     //  });
                        //                 }
                        //
                        //             });
                        //
                        //             //  console.log(this.signupForm.value.phone);
                        //
                        //
                        //             // this.dismiss();
                        //             this.loader.dismiss();
                        //         } else {
                        //             this.toast.show(res.error);
                        //         }
                        //     }, err => {
                        //
                        //         this.loader.dismiss();
                        //         this.toast.show(err.json().error);
                        //     });
                    } else {
                        this.loader.dismiss();
                        this.toast.show(err.json().error);
                    }
                    // console.log(err.json());

                });
        }, err => {

            this.loader.dismiss();
            this.toast.show(err.json().error);
        });
    }

    submitSignupSocial(success) {
        // this.loader.present();
        let tmp = {
            user: success.email,
            pass: 'facebook'
        };
        this.user.nonce('user', 'register').map(x => x.json()).subscribe(x => {
            this.signupForm.value.nonce = x.nonce;
            this.user.signup(this.signupForm.value).map(y => y.json())
                .subscribe((y) => {


                    this.user.login(tmp).map(res => res.json())
                        .subscribe(res => {

                            if (res.status == 'ok') {

                                this.user._loggedIn(res, this.navParams.data.tabIndex);
                                this.user.update_meta(res, 'phone', '966').subscribe();
                                this.user.update_meta(res, 'picture', (success.picture.data.url).replace(new RegExp('&', 'g'), '_')).subscribe();
                                this.user.update_meta(res, 'type_login', 'fb').subscribe();
                                this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                                    this.toast.show(x.REGIST_SUCCESS);
                                });
                                this.dismiss();
                                //   this.loader.dismiss();
                            } else {
                                // this.toast.show(res.error);
                            }
                        }, err => {
                            // this.loader.dismiss();
                            // this.toast.show(err.json().error);
                        });
                }, err => {
                    this.user.login(tmp).map(res => res.json())
                        .subscribe(res => {

                            if (res.status == 'ok') {

                                this.user._loggedIn(res, this.navParams.data.tabIndex);
                                this.user.update_meta(res, 'phone', '966').subscribe();
                                this.user.update_meta(res, 'picture', (success.picture.data.url).replace(new RegExp('&', 'g'), '_')).subscribe();
                                this.user.update_meta(res, 'type_login', 'fb').subscribe();
                                this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                                    this.toast.show(x.REGIST_SUCCESS);
                                });
                                this.dismiss();
                                //   this.loader.dismiss();
                            } else {
                                // this.toast.show(res.error);
                            }
                        }, err => {
                            // this.loader.dismiss();
                            // this.toast.show(err.json().error);
                        });
                    //  this.loader.dismiss();
                    // this.toast.show(err.json().error);
                });
        }, err => {
            this.user.login(tmp).map(res => res.json())
                .subscribe(res => {

                    if (res.status == 'ok') {

                        this.user._loggedIn(res, this.navParams.data.tabIndex);
                        this.user.update_meta(res, 'phone', '966').subscribe();
                        this.user.update_meta(res, 'picture', (success.picture.data.url).replace(new RegExp('&', 'g'), '_')).subscribe();
                        this.user.update_meta(res, 'type_login', 'fb').subscribe();
                        this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                            this.toast.show(x.REGIST_SUCCESS);
                        });
                        this.dismiss();
                        //   this.loader.dismiss();
                    } else {
                        // this.toast.show(res.error);
                    }
                }, err => {
                    // this.loader.dismiss();
                    // this.toast.show(err.json().error);
                });
            // this.loader.dismiss();
            // this.toast.show(err.json().error);
        });
    }

    submitReset() {
        this.loader.present();
        this.user.reset(this.resetForm.value).map(res => res.json())
            .subscribe((res) => {
                if (res.status == 'ok')
                    this.toast.show(res.msg);
                else
                    this.toast.show(res.error);
                this.loader.dismiss();
            }, err => {
                this.loader.dismiss();
                this.toast.show(err.json().error);
            });
    }

    submitLogin() {
        this.loader.present();
        // this.user.login(this.loginForm.value).map(res => res.json())
        //     .subscribe((res) => {
        //
        //         if (res.status == 'ok') {
        //             this.user._loggedIn(res, this.navParams.data.tabIndex);
        //             this.user.get_meta('phone');
        //             this.user.get_meta('picture');
        //             this.user.get_meta('type_login');
        //             this.translate.get(['LOGIN_SUCCESS'], {value: this.user.name}).subscribe(x => {
        //                 this.toast.show(x.LOGIN_SUCCESS);
        //             });
        //             this.dismiss();
        //         } else
        //             this.toast.show(res.error);
        //         this.loader.dismiss();
        //     }, err => {
        //         this.loader.dismiss();
        //         this.toast.show(err.json().error);
        //     });
        let tmp = {
            user: this.loginForm.value.user,
            pass: this.loginForm.value.user
        };

        this.user.login(tmp).map(res => res.json())
            .subscribe(res => {

                if (res.status == 'ok') {
                    let vc = this.getRandomInt(10000, 99999);
                    // let vc = '93118';
                    // this.user._loggedIn(res, this.navParams.data.tabIndex);
                    //this.user.get_meta_verify(res, 'verification').map(ver => ver.json()).subscribe((ver) => {


                    // if (ver['verification'][0] == 'no') {
                    // this.user.update_meta(res, 'phone', this.signupForm.value.phone).subscribe();
                    // this.user.update_meta(res, 'picture', this.signupForm.value.picture).subscribe();
                    // this.user.update_meta(res, 'type_login', this.signupForm.value.type_login).subscribe();
                    // this.user.update_meta(res, 'verification', 'no').subscribe();
                    this.user.update_meta(res, 'verification_code', vc).subscribe();


                    // this.translate.get(['REGIST_SUCCESS'], {value: this.signupForm.value.name}).subscribe(x => {
                    //     this.toast.show(x.REGIST_SUCCESS);
                    // });
                    this.goToVerification(this.loginForm.value.user);
                    this.user.sendSMS('966' + this.loginForm.value.user, 'كود التفعيل الخاص بك للحوم لفطوم هو ' + vc).subscribe((res) => {
                        // this.user.sendSMS('966' + this.loginForm.value.user, 'Your verification code of Fatoum ' + vc).subscribe((res) => {

                        this.translate.get(['VERIFICATION_MSG_SENT'], {value: ''}).subscribe(x => {
                            this.toast.show(x.VERIFICATION_MSG_SENT);
                        });

                    });
                    // } else {
                    //     this.user._loggedIn(res, this.navParams.data.tabIndex);
                    //     this.user.get_meta('phone');
                    //     this.user.get_meta('picture');
                    //     this.user.get_meta('type_login');
                    //     this.translate.get(['LOGIN_SUCCESS'], {value: this.user.name}).subscribe(x => {
                    //         this.toast.show(x.LOGIN_SUCCESS);
                    //     });
                    //     this.dismiss();
                    // }

                    // });

                    // this.dismiss();
                    this.loader.dismiss();
                } else {
                    this.toast.show(res.error);
                    this.loader.dismiss();
                }
            }, err => {

                this.loader.dismiss();
                this.toast.show(err.json().error);
            });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    // fb_login() {
    //     this.facebook.login(['public_profile', 'user_friends', 'email'])
    //         .then((res: FacebookLoginResponse) => {
    //             console.log('Logged into Facebook!', res);
    //             if (res.status == "connected") {
    //
    //                 let successCallback = (success) => {
    //                     this.signupForm = this.fb.group({
    //                         name: [success.name, Validators.required],
    //                         user: [success.email, Validators.required],
    //                         email: [success.email, Validators.email],
    //                         phone: ['966', Validators.required],
    //                         pass: ['facebook', Validators.required],
    //                         pass2: ['facebook', Validators.required],
    //                         picture: [(success.picture.data.url).replace(new RegExp('&', 'g'), '_')],
    //                         type_login: ['fb']
    //                     });
    //                     this.submitSignupSocial(success);
    //                     // this.submitLogin();
    //
    //                     console.log(success);
    //                 };
    //                 let errorCallback = (e) => console.error(e);
    //                 this.facebook.api(res.authResponse.userID + "/?fields=id,email,name,picture,gender", ["public_profile"]).then(successCallback).catch(errorCallback);
    //             }
    //         })
    //         .catch(e => console.log('Error logging into Facebook', e));
    //
    //
    //     // this.facebook.logEvent(this.facebook.EVENTS.EVENT_NAME_ADDED_TO_CART);
    // }


}

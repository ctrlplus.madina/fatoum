import { Component } from '@angular/core';
import { ViewController, App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartProvider, LoadingProvider, UserProvider } from '../../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-mini-cart',
  templateUrl: 'mini-cart.html',
})
export class MiniCartPage {
  total: number = 0;
  isCheckout: boolean = false;
  
  constructor(private user: UserProvider, private cart: CartProvider, private appCtrl: App, public loader: LoadingProvider, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.isCheckout = this.navParams.data.isCheckout;
    let product = this.navParams.data.product;
    let qty = this.navParams.data.qty;
    if(this.navParams.data.product)
      console.log(qty);
      this.cart.post(product, qty);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
  
  goCheckout(){
    if(this.user.all){
      this.dismiss();
      this.appCtrl.getRootNav().push('CheckoutPage');
    }
    else{

            this.navCtrl.push('LoginPage');

      // this.translate.get(['CHECKOUT_GUEST', 'CHECKOUT_GUEST_MSG', 'NO', 'YES']).subscribe( x=> {
      //   this.alert.create({
      //     title: x.CHECKOUT_GUEST,
      //     message: x.CHECKOUT_GUEST_MSG,
      //     buttons: [{
      //         text: x.NO,
      //         handler: () => {
      //           this.dismiss();
      //           this.appCtrl.getRootNav().push('CheckoutPage');
      //         }
      //       },{
      //         text: x.YES,
      //         handler: () => {
      //           this.dismiss();
      //           this.modal.create('LoginPage').present();
      //         }
      //       }]
      //   }).present();
      // });
    }
  }



}

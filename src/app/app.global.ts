export const App: any = {
  store           : 'Fatoum',  // change this with your app name
  ContactEmail    : 'ahmedr@ctrl-plus.com', // change this with your email contact
  url             : 'http://aramcall.com/fatoum', // change this with your URL (please use https, recommended)
  consumerKey     : 'ck_23cc79a336498bc39c23f78ff01c73f6a777d3ab', // change this with your Consumer Key from WooCommerce
  consumerSecret  : 'cs_ec3dd9f7237e3d0b43d8a65e5896074f11886c89', // change this with your Consumer Secret from WooCommerce

  // paypalSandboxClientID: 'AZjyISbp1zmOhZ0o_iAG3W2IGjlz2hvEC-8cGoQ7fXcMFN9afaRuW0X1B1PVSgkSuTQWOKqM9N4NTkOP',
  paypalSandboxClientID: 'AS8NvqS65QkNuLn2UYY6RGUNYPYTO8DfkeZdkyrI7Kk-WF0AxGXCO1qll1RFlAiZbkN8yBx_XBy2wm3o',
  paypalLiveClientID: 'AU1X1l9YuFIV9V4b3ymm9Ha8ES2BxWzxsb_GgH7oiFAC7Ln8P8nLLPpygf7aVnL_QRE2twv-Wx9NY5zg', // get this from paypal developer dashboard
  paypalState: 'PayPalEnvironmentSandbox', // change this to 'PayPalEnvironmentProduction' if you wanna live

  languages: [
    // {id: 'en', title: 'English'},
    // {id: 'id', title: 'Indonesian'},
    // {id: 'fr', title: 'French'},
    // {id: 'in', title: 'Hindi'},
    // {id: 'cn', title: 'Chinese'},
    {id: 'ar', title: 'Arabic'}
  ],

  defaultLang: 'ar'
};
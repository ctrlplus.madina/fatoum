import {SharedModule} from './shared.module';

import {NgModule, ErrorHandler} from '@angular/core';
import {IonicApp, IonicModule, IonicErrorHandler} from 'ionic-angular';
import {MyApp} from './app.component';

import {MODULES, PROVIDERS} from './app.imports';

import {CartPageModule} from "../pages/cart/cart.module";
import {AccountPageModule} from "../pages/account/account.module";
import {OrdersPageModule} from "../pages/orders/orders.module";

@NgModule({
    declarations: [
        MyApp,
    ],
    imports: [
        MODULES,
        SharedModule,
        CartPageModule,
        AccountPageModule,
        OrdersPageModule,
        IonicModule.forRoot(MyApp, {
            tabsHideOnSubPages: true,
            backButtonText: ''
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
    ],
    providers: [
        PROVIDERS,
        // Keep this to enable Ionic's runtime error handling during development
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {

}

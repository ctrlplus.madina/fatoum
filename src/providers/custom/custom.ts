import { Injectable } from "@angular/core";
import "rxjs/add/operator/map";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {ErrorObservable} from "rxjs/observable/ErrorObservable";

@Injectable()
export class CustomProvider {

    constructor(private http: HttpClient) {
    }

    postData(url,data){

        return this.http.post('http://aramcall.com/fatoum/'+url,data)
            .pipe(
                catchError(this.handleError)
            );
    }

    getData(url){
        return this.http.get('http://aramcall.com/fatoum/'+url)
            .pipe(
                catchError(this.handleError)
            );
    }


    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.log(error.error);
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an ErrorObservable with a user-facing error message
        return new ErrorObservable(
            'Something bad happened; please try again later.');

    };




}


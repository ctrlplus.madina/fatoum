import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Http, Response} from '@angular/http';
import { Events } from 'ionic-angular';
import 'rxjs/add/operator/map';

import { App } from '../../app/app.global';


@Injectable()
export class UserProvider {
  private USER_KEY: string = 'user';

  user: any = {};
  _readyPromise: Promise<any>;

  constructor(public storage: Storage, public events: Events, private http: Http) {
    this.load();
    App.url = App.url+'/api';
    // console.log(App.url);
  }

  load() {
    return this.storage.get(this.USER_KEY).then((val) => {
      if (val)
        this._loggedIn(val, 0);

    });
  }

  _loggedIn(user, index){


    this.user = user;
    this.get_meta('phone');
    this.get_meta('picture');
    this.get_meta('type_login');
    this.get_meta('verification');
    this.get_meta('verification_code');
    this.save();
    this.events.publish('user:login', {tabIndex: index});

    return this.user;
  }

  login(data: any){
    let seq = this.http.get(App.url+'/user/generate_auth_cookie/?insecure=cool&username='+data.user+'&password='+data.pass);
    seq.map((res:Response) => {});
    return seq;
  }

  reset(data: any){
    let seq = this.http.get(App.url+'/user/retrieve_password/?insecure=cool&user_login='+data.email);
    seq.map((res:Response) => {});
      
    return seq;
  }

  signup(data: any){
        let seq = this.http.get(App.url+'/user/register/?insecure=cool&nonce='+data.nonce+'&first_name='+data.name+'&display_name='+data.name+'&username='+data.phone+'&user_pass='+data.phone+'&email='+data.email+'&user_status=1');

        seq.map((res:Response) => res.json());

        return seq;
    }

    get_meta(meta_key:any){
        let cookie = this.user.cookie;
        let seq = this.http.get(App.url+'/user/get_user_meta/?insecure=cool&cookie='+cookie+'&meta_key='+meta_key);
        seq.map(res => res.json())
            .subscribe((res) => {

                if (res.status == 'ok'){
                    this.user.user[meta_key] = res[meta_key][0];

                }
            });

        return this.save();
    }

    get_meta_verify(res,meta_key:any){

        let seq = this.http.get(App.url+'/user/get_user_meta/?insecure=cool&cookie='+res.cookie+'&meta_key='+meta_key);
        seq.map((res:Response) => {});
        return seq;
    }

    update_meta(res,meta_key:any,meta_value:any){


        let seq = this.http.get(App.url+'/user/update_user_meta/?insecure=cool&cookie='+res.cookie+'&meta_key='+meta_key+'&meta_value='+meta_value );
        seq.map((res:Response) => {res.json(); });

        //this.user.user[meta_key] = meta_value;
        return seq;
    }

    // update_meta_verify(meta_key:any,meta_value:any){
    //
    //
    //     let seq = this.http.get(App.url+'/user/update_user_meta/?insecure=cool&cookie='+res.cookie+'&meta_key='+meta_key+'&meta_value='+meta_value );
    //     seq.map((res:Response) => {res.json(); });
    //
    //     //this.user.user[meta_key] = meta_value;
    //     return seq;
    // }

    sendSMS(pnumber,message){
        // "https://mobily.ws/api/msgSend.php?apiKey=895e8660d3fe5130ca46cd16c2518e99&numbers=966597796224&lang=3&sender=0541414960&msg=مرحبا بالعالم &applicationType=68&domainName=aramcall.com/fatoum"

        // let seq = this.http.get('https://www.hisms.ws/api.php?send_sms&username=966541414960&password=ؤفقم@123&numbers='+pnumber+'&sender=FATOUM&message='+message );

        let seq = this.http.get('https://mobily.ws/api/msgSend.php?apiKey=895e8660d3fe5130ca46cd16c2518e99&numbers='+pnumber+'&lang=3&sender=FATOUM&msg='+message+'&applicationType=68&domainName=aramcall.com/fatoum' );
        seq.map((res:Response) => {res.json(); });

        return seq;
    }

  update(data: any){
    let cookie = this.user.cookie;

    let f = data.first ? '&first_name='+data.first : '';
    let l = data.last ? '&last_name='+data.last : '';
    let b = data.bio ? '&description='+data.bio : '';
    let p = data.phone ? '&phone='+data.phone : '';



    let seq = this.http.get(App.url+'/user/update_user_meta_vars/?insecure=cool&cookie='+cookie+f+l+b+p );
    seq.map((res:Response) => res.json());

    return seq;
  }

  nonce(controller, method){
    let seq = this.http.get(App.url+'/get_nonce/?controller='+controller+'&method='+method);
    seq.map((res:Response) => res.json());

    return seq;
  }

  isLoggedIn(){
    let cookie = this.user.cookie;
    let seq = this.http.get(App.url+'/user/validate_auth_cookie/?insecure=cool&cookie='+cookie);
    seq.map((res:Response) => res.json());
    return seq;
  }

  logout(){
    this.user = {};
    return this.storage.remove(this.USER_KEY).then(() => {
      this.events.publish('user:logout');
    });
  }

  setUserUpdate(data: any){

    if(data.first)
      this.user.user.firstname = data.first;
    if(data.last)
      this.user.user.lastname = data.last;
    if(data.bio)
      this.user.user.description = data.bio;
      if(data.phone)
          this.user.user.phone = data.phone;
    
    return this.save();
  }

  get email(){
    if(this.user.user.email)
      return this.user.user.email;
  }

  get name(){
    if(this.user.user.displayname)
      return this.user.user.displayname;
  }

  get firstname(){
    if(this.user.user.firstname)
      return this.user.user.firstname;
  }

  get lastname(){
    if(this.user.user.lastname)
      return this.user.user.lastname;
  }

    get phone(){
        if(this.user.user.phone)
            return this.user.user.phone;
    }

    get picture(){
        if(this.user.user.picture)
            return this.user.user.picture;
    }

    get type_login(){
        if(this.user.user.type_login)
            return this.user.user.type_login;
    }



  get displayname(){
    if(this.user.user.displayname)
      return this.user.user.displayname;
  }


  get id(){
    if(this.user.user.id)
      return this.user.user.id;
  }

  get username(){
    if(this.user.user.username)
      return this.user.user.username;
  }

  get cookie(){
    if(this.user.cookie)
      return this.user.cookie;
  }

  get all(){


    if(this.user)
      return this.user.user;
  }

  save(){

    return this.storage.set(this.USER_KEY, this.user);
  }



}

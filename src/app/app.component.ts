import {Component, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Nav, Platform /*,Config*/} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {WooCommerceProvider, SettingsProvider} from '../providers/providers';
import {App} from './app.global';
import {Firebase} from "@ionic-native/firebase";
import {HomePage} from "../pages/home/home";
import {CartPage} from "../pages/cart/cart";
import {AccountPage} from "../pages/account/account";
import {OrdersPage} from "../pages/orders/orders";
import {CartProvider} from "../providers/cart/cart";
import {SocialSharing} from "@ionic-native/social-sharing";
import {AppRate} from "@ionic-native/app-rate";
import {AppVersion} from "@ionic-native/app-version";

@Component({
    templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage: any = 'HomePage';
    app: any = {};
    pages: Array<{ title: string, component: any, icon: string }>;
    packageName: any

    constructor(private platform: Platform, public settings: SettingsProvider, private translate: TranslateService, private woo: WooCommerceProvider, private statusBar: StatusBar, private splashScreen: SplashScreen,
                private firebase: Firebase,public cart: CartProvider,private appRate: AppRate, private socialSharing: SocialSharing,private appVersion: AppVersion) {
        this.settings.load().then((x) => {
            this.woo.loadZones();
            this.app = x;
            this.initTranslate();


            // used for an example of ngFor and navigation
            this.pages = [
                {title: 'HOME', component: HomePage, icon: 'home'},
                {title: 'CART', component: CartPage, icon: 'basket'},
                {title: 'ORDERS', component: OrdersPage, icon: 'list-box'},
                {title: 'ACCOUNT', component: AccountPage, icon: 'contact'},
                // {title: 'WISHLIST', component: WishlistPage, icon: 'heart'},
                {title: 'SHARE', component: 'SharePage', icon: 'share'},
                {title: 'RATE', component: 'RatePage', icon: 'star-half'}
            ];

        });

        platform.ready().then(() => {
            this.statusBar.overlaysWebView(false);
            this.statusBar.backgroundColorByHexString('#173763');
            this.splashScreen.hide();
            // if (platform.is('cordova')) {
            //   this.oneSignal.startInit(App.OneSignalAppID, App.GCM);
            //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
            //
            //   this.oneSignal.handleNotificationReceived().subscribe((x) => {
            //   // do something when notification is received
            //     console.log(x);
            //     this.notif.post(x.payload);
            //   });
            //
            //   this.oneSignal.handleNotificationOpened().subscribe(() => {
            //     // do something when a notification is opened
            //   });
            //
            //   this.oneSignal.endInit();
            // }

            if (this.platform.is('cordova')) {
                // this.isApp = false;
                this.getPackageName();
                this.firebase.getToken()
                    .then(token => {
                        }
                    )
                    // save the token server-side and use it to push notifications to this device
                    .catch(error => {
                        }
                    );

                this.firebase.onTokenRefresh()
                    .subscribe((token: string) => {
                    });

                //
                // this.firebase.onNotificationOpen().subscribe((notification: string) => {
                //     this.notif.post(notification);
                // });

            }
        });
    }


    // ionViewDidLoad() {
    //   this.platform.ready().then(() => {
    //     // this.statusBar.styleDefault();
    //
    //   });
    // }

    // ionViewCanEnter(){
    //
    // }

    initTranslate() {
        // Set the default language for translation strings, and the current language.
        this.translate.setDefaultLang(this.app && this.app.language !== undefined ? this.app.language : App.defaultLang);

        if (this.app && this.app.language !== undefined)
            this.translate.use(this.app.language);
        else
            this.translate.use(App.defaultLang);

        // this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
        //   this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        // });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.component === 'SharePage') {
           this.regularShare();
        } else if (page.component === 'RatePage') {
           this.rate();

        } else {
            this.nav.setRoot(page.component);
        }


    }

    async getPackageName() {
        this.packageName = await this.appVersion.getPackageName();

    }

    rate() {

        this.appRate.preferences.storeAppURL = {
            ios: 'com.fatoum.ctrlplus', // FOR IOS
            android: 'market://details?id=' + this.packageName,
            // windows: 'ms-windows-store://review/?ProductId=' + this.packageName,
        };

        this.appRate.promptForRating(true);
    }

    regularShare() {

        // var msg = 'مرحبًا ، جرب هذا التطبيق.\n' +
        //     '\n\n' +
        //     'Android App:\n' +
        //     'https://play.google.com/store/apps/details?id=' + this.packageName +
        //     '\n\n' +
        //     'Apple App:\n' +
        //     'https://itunes.apple.com/us/app/%D9%84%D8%AD%D9%88%D9%85-%D9%81%D8%B7%D9%88%D9%85-fatoum/id1422890010';

        var msg = 'مرحبًا ، جرب هذا التطبيق.\n';

        if (this.platform.is('android')) {
            this.socialSharing.share(msg, 'مشاركة الرابط!\n', null, 'https://play.google.com/store/apps/details?id=' + this.packageName);
        }

        if (this.platform.is('ios')) {
            this.socialSharing.share(msg, 'مشاركة الرابط!\n', null, 'https://itunes.apple.com/us/app/%D9%84%D8%AD%D9%88%D9%85-%D9%81%D8%B7%D9%88%D9%85-fatoum/id1422890010');
        }
    }

}

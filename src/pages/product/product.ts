import {Component, NgZone} from '@angular/core';
import {IonicPage, NavController, ModalController, NavParams, Platform} from 'ionic-angular';
import {WishlistProvider, ToastProvider, WooCommerceProvider} from '../../providers/providers';
import {TranslateService} from '@ngx-translate/core';
import {CartProvider} from "../../providers/cart/cart";
import {DatePicker} from "@ionic-native/date-picker";
import {DatePipe} from "@angular/common";

@IonicPage()
@Component({
    selector: 'page-product',
    templateUrl: 'product.html',
})

export class ProductPage {
    product: any;

    variations: any[] = [];
    reviews: any[] = [];
    related: any[] = [];
    isSetVariation: boolean = false;
    cutType: any;
    attribute: any[] = [];
    quantity: any;
    zone: NgZone;
    datepickerCounter: any;
    comingSoon: any;

    constructor(private translate: TranslateService, private toast: ToastProvider, private wishlist: WishlistProvider, private navCtrl: NavController, private modal: ModalController, private navParam: NavParams, private woo: WooCommerceProvider, private cart: CartProvider,
                private datePicker: DatePicker, private platform: Platform) {
        //  this.loader.present();
        this.comingSoon = false;
        this.zone = new NgZone({enableLongStackTrace: false});
        this.product = this.navParam.data.params;
        if(this.product.variations.length > 0){
            this.product.price = 0;
        }

        if (this.product.short_description.indexOf('<p>قريبا</p>') == -1) {
            this.comingSoon = false;
        }else{
            this.comingSoon = true;
        }
        //this.history.post(this.product);
        this.datepickerCounter = 1;
        this.cutType = 'نوع التقطيع';
        this.attribute = this.product.attributes;
        if (this.attribute.length > 0) {

            this.attribute.forEach(function (value) {
                // console.log(value);
                value.option = "";
            });


        }


        this.quantity = 1;
        if (this.product.variations.length > 0) {


            this.woo.getProductVariations(this.product.id).then((val) => {
                this.zone.run(() => {
                    this.product.variations = val;
                    //console.log(this.product.variations);
                });

            })
        }

        if (this.product.rating_count > 0) {
            this.woo.getProductReviews(this.product.id).then((val) => {
                this.product.reviews = val;
                //this.loader.dismiss();
            })
        }

        // this.product.related = this.woo.getProductRelated(this.product.related_ids);
        // this.loader.dismiss();
    }


    changeAttributes(att_name) {

        if (att_name == 'اليوم') {

            let alyom = this.attribute.filter(item => item.name == 'اليوم');

            if (alyom[0].option == 'اخرى' || alyom[0].option.indexOf("/") > 0) {

                this.datepickerCounter = this.datepickerCounter + 1;
                if (this.datepickerCounter % 2 == 0 || alyom[0].option == 'اخرى') {
                    this.translate.get(['YES', 'NO']).subscribe(x => {
                        let minDate = this.platform.is('ios') ? new Date() : (new Date()).valueOf();


                        // if(this.platform.is('ios')){
                        //     let minDate = new Date();
                        //     minDate.setDate( minDate.getDate() + 1 );
                        //
                        // }else{
                        //     let minDate = new Date();
                        //     minDate.setDate( minDate.getDate() + 1 );
                        //
                        // }


                        this.datePicker.show({
                            date: new Date(),
                            mode: 'date',
                            androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT,
                            minDate: minDate,
                            cancelText: x.NO,
                            okText: x.YES
                        }).then(
                            date => {

                                let pipe = new DatePipe('en-US');
                                const myFormattedDate = pipe.transform(date, 'shortDate');
                                this.product.attributes.forEach(function (value) {
                                    // console.log(value);
                                    if (value.name == "اليوم") {
                                        value.options.forEach(function (option) {
                                            if (option == "اخرى" || option.indexOf("/") > 0) {
                                                value.options[2] = myFormattedDate;
                                            }
                                        });

                                    }
                                });
                                alyom[0].option = myFormattedDate;
                            },
                            err => {
                            }
                        );

                    });
                }
            }
        }

    }

    quantityControl(type) {


        if (type == 'add') {
            this.quantity++;
        } else {
            if (this.quantity > 1) {
                this.quantity--;
            }
        }
    }

    setVariation(e) {

        if (e) {


            let x = JSON.parse(e);

            this.product.variation_id = x.id;
            this.product.price = x.price;
            this.product.regular_price = x.regular_price;
            this.product.on_sale = x.on_sale;
            this.product.in_stock = x.in_stock;

            // this.attribute.forEach(function (value) {
            //     // console.log(value);
            //     x.attributes.push(value);
            // });
            // console.log(x.attributes);
            this.attribute.forEach(function (value) {
                // console.log(value);

                if (value.id == 4) {
                    value.option = x.attributes[0].option;
                }

            });

            this.product.attributes = this.attribute;

            this.isSetVariation = true;
        }else{
            this.isSetVariation = false;
            this.product.price = 0;
    }

    }

    viewCart() {
        this.modal.create('MiniCartPage', {}, {cssClass: 'inset-modal'}).present();
    }

    goToRoot(page, params) {
        this.navCtrl.setRoot(page, {params: params});
    }

    openModal(pageName) {

        if (this.product.variations.length > 0 && !this.isSetVariation) {
            this.translate.get(['SELECTPRODUCTFILTER'], {value: 'الحجم'}).subscribe(x => {
                this.toast.show(x.SELECTPRODUCTFILTER);
            });
        }
        else if (this.product.attributes.length > 0) {

            if (this.product.attributes.length == 1) {
                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }
            }

            if (this.product.attributes.length == 2) {
                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[1].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[1].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }
            }

            if (this.product.attributes.length == 3) {
                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[1].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[1].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[2].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[2].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }
            }

            if (this.product.attributes.length == 4) {
                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[1].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[1].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[2].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[2].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[3].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[3].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }
            }

            if (this.product.attributes.length == 5) {
                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[1].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[1].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[2].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[2].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[3].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[3].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[4].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[4].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }
            }

            if (this.product.attributes.length == 6) {

                if (this.product.attributes[0].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[0].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                } else if (this.product.attributes[1].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[1].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }

                else if (this.product.attributes[2].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[2].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else if (this.product.attributes[3].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[3].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else if (this.product.attributes[4].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[4].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else if (this.product.attributes[5].option == '') {
                    this.translate.get(['SELECTPRODUCTFILTER'], {value: this.product.attributes[5].name}).subscribe(x => {
                        this.toast.show(x.SELECTPRODUCTFILTER);
                    });
                }
                else {
                    this.cart.post(this.product, this.quantity);
                    this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                        this.toast.show(x.ADDED_TO_CART);
                    });
                }

            }


        } else {
            this.cart.post(this.product, this.quantity);
            // this.product.quantity = this.quantity;
            // this.modal.create(pageName, {
            //     product: this.product,
            //     qty: this.quantity
            // }, {cssClass: 'inset-modal'}).present();
            this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                this.toast.show(x.ADDED_TO_CART);
            });
        }
    }

    setFav(product: any) {
        this.translate.get(['REMOVE_WISH', 'ADDED_WISH']).subscribe(x => {
            let msg = product.isFav ? x.REMOVE_WISH : x.ADDED_WISH;
            this.wishlist.post(product);
            product.isFav = product.isFav ? false : true;
            this.toast.show(msg);
        });
    }

    // share(product: any){
    //   // console.log(product);
    //   if (!this.platform.is('cordova')) {
    //     this.translate.get(['OK', 'ONLY_DEVICE', 'ONLY_DEVICE_DESC']).subscribe( x=> {
    //       this.alert.create({
    //         title: x.ONLY_DEVICE,
    //         message: x.ONLY_DEVICE_DESC,
    //         buttons: [{
    //             text: x.OK
    //           }]
    //       }).present();
    //       return false;
    //     });
    //
    //   }else{
    //     let img = [];
    //     for(let i in product.images)
    //       img.push(product.images[i].src);
    //
    //     this.socialSharing.share(product.name, product.name, img, product.permalink).then((x) => {
    //       console.log(x);
    //       this.toast.show('Successfully shared');
    //     }).catch((err) => {
    //       console.log(err);
    //     });
    //   }
    // }

    goTo(page: string, params: any) {
        this.navCtrl.push(page, {params: params});
    }

    // mydatepicker() {
    //     this.datePicker.show({
    //         date: new Date(),
    //         mode: 'date',
    //         androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    //     }).then(
    //         date => console.log('Got date: ', date),
    //         err => console.log('Error occurred while getting date: ', err)
    //     );
    // }

}
